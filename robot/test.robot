*** Settings ***
Documentation     Simple example using SeleniumLibrary.
Library           SeleniumLibrary

*** Variables ***
${WEBSITE URL}      https://search.opensuse.org
${BROWSER}        firefox

*** Test Cases ***
Valid Search
    Open Browser To Default Page
    Input Search    Test
    Submit Search
    Search Page Should Be Open
    [Teardown]    Close Browser

*** Keywords ***
Open Browser To Default Page
    Open Browser    ${WEBSITE URL}    ${BROWSER}
    Title Should Be    openSUSE Search

Input Search
    [Arguments]    ${search text}
    Input Text    searchbox    ${search text}

Submit Search
    Click Element    //span[@class="hidden-xs-down"]

Search Page Should Be Open
    Title Should Be    Test at DuckDuckGo